extern crate csv;
use csv::Writer;
use std::error::Error;
use std::fs::File;

extern crate nalgebra as na;

use na::{Complex, DMatrix, Point2, Matrix2, Matrix};
use crate::na::ComplexField;

use lapack::*;
use lapack_src::*;
use lapack::c64;

use std::io::prelude::*;
use csv::ReaderBuilder;

mod lattice;
mod hamiltonian;


fn compute_eigenvectors(matrix: &mut DMatrix<Complex<f64>>, frequencies: &mut Vec<f64>) -> DMatrix<Complex<f64>> {
    let n:i32 = matrix.ncols() as i32;
    let mut vec_c64: Vec<lapack::c64> = matrix.iter().map(|&c| lapack::c64::new(c.re, c.im)).collect();
    // To be removed in the final version
    let jobvl = b'V'; // 'V' for computing left eigenvectors, 'N' for not computing
    let jobvr = b'V'; // 'V' for sorting eigenvalues, 'N' for not sorting
    
    let mut info:i32  = 0;
    let lda = n;
    
    let mut w = vec![c64::new(0.0, 0.0); n as usize];
    let mut vl = vec![c64::new(0.0, 0.0); n as usize * n as usize];
    let mut vr = vec![c64::new(0.0, 0.0); n as usize * n as usize];
    let mut work = vec![c64::new(0.0, 0.0); n as usize * n as usize];
    let mut rwork = vec![0. as f64; n as usize * n as usize];


    unsafe {
	zgeev(
            jobvl,
            jobvr,
            n,
            &mut vec_c64,
            lda,
            &mut w,
            &mut vl,
            n,
            &mut vr,
            n,
	    &mut work,
	    2*n,
	    &mut rwork,
	    &mut info
	);
    }

    for i in 0..n {
	frequencies.push(-w[i as usize].re/2.);
    }    

    DMatrix::from_iterator(n as usize, n as usize, vr.iter().map(|&c| Complex::new(c.re, c.im)))
}

// fn compute_vxvy(lattice: Vec<Point2<f64>>, psi: &mut DMatrix<Complex<f64>>, k: i32) -> DMatrix<Complex<f64>> {
//     let x_coord: Vec<_> = lattice.iter().map(|&x| x[0]).collect();
//     let y_coord: Vec<_> = lattice.iter().map(|&y| y[1]).collect();
//     let Lx = x_coord.iter().max();
// }

fn search_nearest(t: & mut Vec<f64>, x: f64) -> usize{
    let mut k = 0;
    for i in 0..t.len() {
	if t[i] > x {
	    k=i;break;}
    }
    k 
}



fn main() -> Result<(), Box<dyn Error>> {
    let n_obj = 200;
    let a = 1.;
    let pi = std::f64::consts::PI;
    let k0 = 2.*pi*0.05;
    let mut lattice = lattice::generate_lattice(n_obj, a);
    let n_sites = lattice.len();
    let n_matrix = 4*n_sites*n_sites;

    let mut complex_matrix = hamiltonian::create_green_te(& mut lattice,k0);
    hamiltonian::break_symmetry(&mut complex_matrix,12.,0.);

    let mut frequencies_2:Vec<f64> = Vec::new();
    
    let psi = compute_eigenvectors(& mut complex_matrix, & mut frequencies_2);
    
    let mut indices: Vec<usize> = (0..frequencies_2.len()).collect();
    indices.sort_by_key(|&i| frequencies_2[i] as i64);
    frequencies_2 = indices.iter().map(|&i| frequencies_2[i]).collect();    

    println!("{:?}",frequencies_2);

    // let output_path = "./data/frequences.csv";
    // let file = File::create(output_path)?;
    // let mut writer = Writer::from_writer(file);
    // for f in &frequencies_2 {
    //     writer.write_record(&[f.to_string()])?;
    // }

    let n = complex_matrix.ncols();
    
    let mut psi_sorted = DMatrix::zeros(n, n);
    for (new_col, old_col) in indices.iter().zip(0..n) {
        psi_sorted.column_mut(*new_col).copy_from(&psi.column(old_col));
    }

    let k = search_nearest(& mut frequencies_2, 7.);

    println!("k = {:}", k);

    let x_coord: Vec<_> = lattice.iter().map(|&x| x[0]).collect();
    let y_coord: Vec<_> = lattice.iter().map(|&y| y[1]).collect();
    let lx = x_coord.iter().max_by(|a, b| a.total_cmp(b)).unwrap() - x_coord.iter().min_by(|a, b| a.total_cmp(b)).unwrap();
    let ly = y_coord.iter().max_by(|a, b| a.total_cmp(b)).unwrap() - y_coord.iter().min_by(|a, b| a.total_cmp(b)).unwrap();
    
    let x_coord_2: Vec<_> = x_coord.iter().flat_map(|&x| std::iter::repeat(x).take(2)).collect();
    let y_coord_2: Vec<_> = y_coord.iter().flat_map(|&x| std::iter::repeat(x).take(2)).collect();


    let mut  w = DMatrix::zeros(n,k);

    for i in 0..k {
	w.column_mut(i).copy_from(&psi_sorted.column(i));
    }

    println!("ici000"); 
    let mut phase_x = DMatrix::zeros(n,n);
    let mut phase_y = DMatrix::zeros(n,n);

    for i in 0..n {
	let theta_x = 2.0 * std::f64::consts::PI * x_coord_2[i] / lx;
	let theta_y = 2.0 * std::f64::consts::PI * y_coord_2[i] / ly;
	phase_x[(i,i)] = Complex::new(theta_x.cos(),theta_x.sin());
	phase_y[(i,i)] = Complex::new(theta_y.cos(),theta_y.sin());
    }
    println!("ici00"); 

    let w_dagger = w.transpose().conjugate();
    
    let vx = &w_dagger*phase_x*&w;
    let vy = &w_dagger*phase_y*&w;

    let vx_dagger = vx.transpose().conjugate();
    let vy_dagger = vy.transpose().conjugate();
    println!("ici0"); 
    // let vx_dagger = vx.clone().try_inverse().unwrap();
    // let vy_dagger = vy.clone().try_inverse().unwrap();

    let wilson_loop = vx*vy*vx_dagger*vy_dagger;

    println!("ici1"); 
    
    let eigenvalues_wl = wilson_loop.eigenvalues();

    let mut s = Complex::new(0.,0.);
    
    for x in &eigenvalues_wl.clone().unwrap() {
	s += x.ln();
    }

    println!("{:}", s.im/2./std::f64::consts::PI);

    // println!("{:?}", eigenvalues_wl.clone().unwrap().len());
    // println!("{:?}", eigenvalues_wl.clone().unwrap());
    
    // Create map for different size of lattice for the manuscript !
    
    Ok(())
}

