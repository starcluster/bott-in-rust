use na::{Complex, DMatrix, Point2, Matrix2, Matrix};
use crate::na::ComplexField;

fn poly_p(x: Complex<f64>) -> Complex<f64> { 1.-1./x+1./x/x}

fn poly_q(x: Complex<f64>) -> Complex<f64> { -1.+3./x-3./x/x}

pub fn create_green_te(lattice: & mut Vec<Point2<f64>>, k0: f64) -> DMatrix<Complex<f64>> {
   let n = lattice.len();
   let mut matrix: DMatrix<Complex<f64>> = DMatrix::zeros(2*n, 2*n);

    for i in 0..n {
        for j in 0..n {
	    if i==j {
		matrix[(2*i,2*j)] = Complex::new(0.,1.);
		matrix[(2*i+1,2*j+1)] = Complex::new(0.,1.);
	    } else {
		let x = (lattice[i].x - lattice[j].x)*k0;
		let y = (lattice[i].y - lattice[j].y)*k0;
		let r = na::distance(&lattice[i],&lattice[j])*k0;
		let one_i_r = Complex::new(0.,r);
		let t1 = 3./2.*one_i_r.exp()/r;
		let t2 = poly_p(one_i_r);
		let t3 = poly_q(one_i_r);
		matrix[(2*i,2*j)] = t1*(t2+t3*x*x/r/r);
		matrix[(2*i,2*j+1)] = t1*t3*x*y/r/r;
		matrix[(2*i+1,2*j)] = t1*t3*x*y/r/r;
		matrix[(2*i+1,2*j+1)] = t1*(t2+t3*y*y/r/r);
	    }
        }
    }
    let sr2 = 1.0 / f64::sqrt(2.0);
    let deg = Matrix2::new(
        Complex::new(sr2, 0.0),
	Complex::new(0.0, sr2),
	Complex::new(-sr2, 0.0),
        Complex::new(0.0, sr2),
    );

    for i in 0..n {
        for j in 0..n {
            if i != j {
                let block = Matrix2::new(matrix[(2*i,2*j)],
					matrix[(2*i,2*j+1)],
					matrix[(2*i+1,2*j)],
					matrix[(2*i+1,2*j+1)]);
                let block2 = deg * block * deg.adjoint();
		matrix[(2*i,2*j)] = block2[(0,0)];
		matrix[(2*i,2*j+1)] = block2[(0,1)];
		matrix[(2*i+1,2*j)] = block2[(1,0)];
		matrix[(2*i+1,2*j+1)] = block2[(1,1)];
		
            }
        }
    }  
    matrix
}

pub fn break_symmetry(matrix: &mut DMatrix<Complex<f64>>, delta_b:f64, delta_ab:f64) {
    for i in 0..matrix.ncols(){
	let mut inversion_symmetry = 1.;
	if i > matrix.ncols()/2 {
	    inversion_symmetry = -1.
	}
	if i%2==0 {
	    matrix[(i,i)] = Complex::new(2.*delta_b+2.*inversion_symmetry*delta_ab,1.);
	} else {
	    matrix[(i,i)] = Complex::new(-2.*delta_b+2.*inversion_symmetry*delta_ab,1.);
	}
    }
}
