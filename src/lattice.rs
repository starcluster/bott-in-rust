use na::{Point2};

pub fn generate_lattice(n:usize, a:f64) -> Vec<Point2<f64>> {
    let n_side = ((n as f64)/2.).sqrt() as usize;
    let b = 3_f64.sqrt()*a;
    let c = 3./2. * a;
    let mut v:Vec<Point2<f64>> = Vec::with_capacity(2*n_side*n_side);
    for i in 0..n_side {
	for j in 0..n_side {
	    let i_f = i as f64;
	    let j_f = j as f64;
	    let p = Point2::new((i_f + 1. / 4. * (1. + (-1_f64).powf(j_f + 1.))) * b, j_f * c);
	    v.push(p);
	}
    }

    for i in 0..n_side*n_side {
	let p = &v[i];
	let q = Point2::new(p.x, p.y + a);
	v.push(q);
    }
    v
}
