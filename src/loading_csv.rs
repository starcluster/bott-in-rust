extern crate csv;
use csv::Writer;
use std::error::Error;
use std::fs::File;

extern crate nalgebra as na;

use na::{Complex, DMatrix, Point2, Matrix2, Matrix};
use crate::na::ComplexField;

use std::io::prelude::*;
use csv::ReaderBuilder;

pub fn load_mat_from_csv(file_path: &str, complex_matrix: & mut DMatrix<f64>) -> Result<(), Box<dyn Error>>{

    // Lire le fichier CSV
    let file = File::open(file_path)?;
    let mut reader = ReaderBuilder::new().has_headers(false).from_reader(file);

    // Stocker les valeurs lues dans un vecteur de vecteurs de nombres complexes
    let mut complex_values: Vec<Vec<f64>> = Vec::new();

    for result in reader.records() {
        let record = result?;
        let mut row: Vec<f64> = Vec::new();

	// println!("{:?}", record);
	// std::process::exit(0);
        for field in record.iter() {
	    let value: f64 = field.parse()?; // Convertir le champ CSV en f64
            row.push(value)
        }

        complex_values.push(row);
    }

    // Convertir le vecteur de vecteurs en DMatrix
    let num_rows = complex_values.len();
    let num_cols = complex_values[0].len();
    // let mut complex_matrix = nalgebra::DMatrix::zeros(num_rows, num_cols);

    for i in 0..num_rows {
        for j in 0..num_cols {
            complex_matrix[(i, j)] = complex_values[i][j];
        }
    }

    println!("Matrice complexe chargée :\n{:?}", complex_matrix);

    Ok(())

}
