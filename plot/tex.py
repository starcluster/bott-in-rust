import matplotlib.pyplot as plt
import numpy as np


def useTex():
    plt.rc("text.latex", preamble=r"\usepackage{amsmath}\usepackage[utf8]{inputenc}")

    plt.rcParams.update(
        {
            "text.usetex": True,
            "font.family": "sans-serif",
            "font.sans-serif": ["Helvetica"],
        }
    )


def f_to_Tex(s):
    """
    Turn a float into TeX material
    """
    return "$\;\;" + str(np.round(s, 2)) + " $"


def t_to_Tex(s):
    """
    Turn a text into TeX material
    """
    return "$\mathrm{" + s + "}$"
