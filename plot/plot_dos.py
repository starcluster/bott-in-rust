import numpy as np
import matplotlib.pyplot as plt

import tex
tex.useTex()

if __name__ == "__main__":
    f = np.genfromtxt("../data/frequences.csv", delimiter=',', dtype=float, encoding=None)
    plt.hist(f, bins=80, histtype="step",color="red")
    plt.xlabel(r"$\textrm{Energy } \omega$",fontsize=20)
    plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig(f"../figs/dos.pdf",format="pdf",bbox_inches='tight')
    plt.show()
